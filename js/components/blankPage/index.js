import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Right,
  Body
} from "native-base";
import MapView from 'react-native-maps';

import styles from "./styles";

class Sayfa extends Component {

  constructor(props) {
    super(props)
    this.createContent = this.createContent.bind(this)
  }

  static navigationOptions = {
    header: null
  };

  static propTypes = {
    name: React.PropTypes.string,
    index: React.PropTypes.number,
    list: React.PropTypes.arrayOf(React.PropTypes.string),
    openDrawer: React.PropTypes.func
  };


  createContent(item) {
    let titleContent = "";
    if(item === "Balık Seç - Mera Bul !") {
      titleContent = "Balık Seçerek Mera Bulma ";
    } else if (item === "Mevsim Seç - Mera Bul !") {
       titleContent = "Mevsim Seçerek Mera Bulma ";
    } else {
      titleContent = "Yem Seçerek Mera Bulma ";
    }

    return (
      <Content padder>
        <Text>
          {titleContent}Sayfasına Hoşgeldin . . .
        </Text>
        <MapView
          initialRegion={{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
        />
      </Content>
    );
  }

  render() {
    const { props: { name, index, list } } = this;
    console.log(this.props.navigation, "000000000");
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" />
            </Button>
          </Left>

          <Body>
            <Title>Mera Listesi</Title>
          </Body>

          <Right />
        </Header>

        {this.createContent(this.props.navigation.state.params.name.item)}

      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer())
  };
}

const mapStateToProps = state => ({
  name: state.user.name,
  index: state.list.selectedIndex,
  list: state.list.list
});

export default connect(mapStateToProps, bindAction)(Sayfa);
